# frozen_string_literal: true

require 'digest'
require 'json'
require 'leveldb'
require 'sinatra/base'

# Main class for api endpoints
class HashDbApi < Sinatra::Base
  enable :lock
  set :database_prefix, '/databases/'
  set :protection, except: [:json_csrf]

  before do
    content_type :json
  end

  get '/' do
    'Nobody here but us chickens'.to_json
  end
end

require_relative 'routes/v1'
