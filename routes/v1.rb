# frozen_string_literal: true

# Main class for v1 of API
class HashDbApi < Sinatra::Base
  put '/v1/insert' do
    # Check if password is set
    halt 400 if params['password'].nil?

    # Calculate hashes
    hashes = {
      md5:    (Digest::MD5.hexdigest params['password']),
      sha1:   (Digest::SHA1.hexdigest params['password']),
      sha256: (Digest::SHA256.hexdigest params['password'])
    }

    # Insert hashes into database
    hashes.each do |algo, hash|
      db = LevelDB::DB.new "#{settings.database_prefix}#{algo}.db"
      db.put hash, params['password']
      db.close
    end

    # Add password to hashes to construct results
    hashes['password'] = params['password']

    # Show results
    hashes.to_json
  end

  get '/v1/:algo/:hash' do
    # Check if valid hahshing algorithm
    halt 404 unless %w[md5 sha1 sha256].include? params['algo']

    # Check if it is a valid hash
    params['hash'].downcase!
    size = { 'md5' => 32, 'sha1' => 40, 'sha256' => 64 }[params['algo']]
    halt 400 if /^[0-9a-f]{#{size}}$/.match(params['hash']).nil?

    # Attach to database
    db = LevelDB::DB.new "#{settings.database_prefix}#{params['algo']}.db"

    # Perform lookup
    result = db.get params['hash']

    # Release database lock
    db.close

    # Format and return result
    {
      'hash':   params['hash'],
      'found':  !result.nil?,
      'result': result
    }.to_json
  end
end
